<?php

namespace App\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class AdvertExpiryCommandCommand extends ContainerAwareCommand
{
    # Run every five minutes
    protected $idenfitiers = [
        'ExpiryFifteen'     => "Y-m-d 01:00",
        'ExpiryThirteen'    => "Y-m-d 02:00",
        'ExpiryThirty'      => "Y-m-d 03:00",
        'ExpiryTwentyEight' => "Y-m-d 04:00",
    ];

    protected function configure()
    {
        $this
            ->setName('advert:expiry-command')
            ->setDescription('...')
            ->addArgument('expiry', InputArgument::OPTIONAL, 'Expiry Identifier')
            ->addOption('option', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $expiry = $input->getArgument('expiry');

        if (!$expiry) {
            $expiry = $this->getIdentifiersByTime();
        }

        if ($input->getOption('option')) {

        }

        $output->writeln([
            'Expiry message sender',
            '======================',
            '',
        ]);
        if (!isset($this->idenfitiers[$expiry])) {
            $message = 'Please enter a valid idenfitier e.g ' . implode(', ', array_keys($this->idenfitiers));
        } else {
            $cron_manager = $this->getContainer()->get('app.cron_manager');
            $count = $cron_manager->advertExpiry($expiry);
            $message = $count . ' Expiry message(s) sent. For expiry: ' . $expiry;
        }
        $logger = $this->getContainer()->get('logger');
        $logger->info($message);

        $output->writeln($message);
    }


    /**
     * Get the class name based on time if not is given in console
     * 
     * @return mixed
     */
    protected function getIdentifiersByTime()
    {
        $time = time();

        foreach ($this->idenfitiers as $idenfitier => $datetime) {
            $datetime = strtotime($datetime);
            if ($time >= $datetime &&  $time < ($datetime + 1800)) {
                return $idenfitier;
            }
        }

        return false;
    }

}
