<?php
namespace App\Cron;

interface Job 
{
	public function handle();
}