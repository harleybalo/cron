<?php

namespace App\Cron\Advert;
use App\Cron\Advert\Expiry;
use Doctrine\ORM\EntityManager;

class ExpiryThirty implements Expiry
{
	public $limit = 2000;

	public function getTemplate()
	{
		return "MarketBundle:Emails:expirefifteen.html.twig";
	}

	public function getAdverts(EntityManager $em)
	{
		$expiry   =  30;
		$featured = true;
		return $em->getRepository('MarketBundle:Advert')
            ->finAllByToBeExpired($expiry, $featured)
        ;
	}

	public function setStatus()
	{
		return 'Expired';
	}
}