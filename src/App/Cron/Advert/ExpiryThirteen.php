<?php

namespace App\Cron\Advert;
use App\Cron\Advert\Expiry;
use Doctrine\ORM\EntityManager;

class ExpiryThirteen implements Expiry
{
	public $limit = 2000;

	public function getTemplate()
	{
		return "MarketBundle:Emails:expirethirteen.html.twig";
	}

	public function getAdverts(EntityManager $em)
	{
		$expiry =  2;
		return $em->getRepository('MarketBundle:Advert')
            ->finAllByToBeExpired($expiry)
        ;
	}

	public function setStatus()
	{
		return false;
	}
}