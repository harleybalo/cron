<?php

namespace App\Cron\Advert;
use Doctrine\ORM\EntityManager;

interface Expiry
{
	public function getTemplate();

	public function getAdverts(EntityManager $em);

	public function setStatus();
}