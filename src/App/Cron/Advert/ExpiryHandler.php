<?php

namespace App\Cron\Advert;
use App\Cron\Job;
use App\Cron\Advert\Expiry;
use Doctrine\ORM\EntityManager;

class ExpiryHandler implements Job
{

	/**
	 *	Expiry $expiry
	 *
	 * @var object
	 */
	public $expiry;


	/**
	 * Entity Manager $em
	 *
	 * @var object
	 */
	protected $em;

	public function __construct(Expiry $expiry, EntityManager $em, $container) 
	{
		$this->expiry 	= $expiry;
		$this->em 		= $em;
		$this->container = $container;
	}

	public function handle()
	{
		$adverts = $this->expiry->getAdverts($this->em);
		$status  = $this->expiry->setStatus();
        $total   = count($adverts);
		if ($adverts) {
			foreach ($adverts as $key => $advert) {
				$this->mailer($advert);
				if ($status) {
					$advert->setStatus($status);
					$this->em->persist($advert);
					$this->em->flush();
				}
			}
		}

        return $total;
	}

	private function mailer($advert) 
    {
    	if (!$this->expiry->getTemplate()) {
    		throw new \Exception("Email Template is not specified", 1);
    	}

    	//echo $advert->getId(); echo ' - ';

        try {
            $message = \Swift_Message::newInstance()
                ->setSubject('Performance Report')
                ->setFrom('do_not_reply@gidimarket.com')
                ->setTo($advert->getEmail())
                ->setBody(
                    $this->container->get('templating')->render(
                        // app/Resources/views/Emails/registration.html.twig
                        //'MarketBundle:Emails:password_change.html.twig'
                        $this->expiry->getTemplate(), [
                            'name'      => $advert->getName(),
                            'id'        => $advert->getId(),
                            'title'     => $advert->getItemName(),
                            'slug'      => $advert->getSlug(),
                            'today'     => time(),
                        ]
                    ),
                    'text/html'
                )
            ;
            $this->container->get('mailer')->send($message);
        } catch (Exception $e) {
            
        }        
    }
}